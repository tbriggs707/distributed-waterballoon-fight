﻿using Messages.RequestMessages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedObjects;
using System.Net.Sockets;
using System.Net;
using Messages.ReplyMessages;
using Messages;
using System.Threading;
using log4net;
using log4net.Config;
using System.IO;

namespace Player
{
    class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger((typeof(Program)));

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            Logger.Debug("Session has begun");
            Player teran = new Player();
            Logger.Debug("Player has been created");

            teran.Start();

            Console.WriteLine("Press the 'Enter' key to stop playing and logout...");
            Console.ForegroundColor = ConsoleColor.Yellow;

            while (Console.ReadKey(true).Key != ConsoleKey.Enter && teran.Live)
            {
                Console.WriteLine(" Press 'Enter' to exit");
                Thread.Sleep(100);
            }

            Logger.Debug("Program Terminated");
            Console.ResetColor();
            Console.WriteLine("Program Terminating...\nThanks for playing!");
            Thread.Sleep(3000);

            #region OLD CODE
            // Written Ad-hoc, was later refactored for better organization
            // Variables
            //var configs = ConfigurationManager.AppSettings;
            //UdpClient client = new UdpClient();
            //string[] ip_port = configs["RegistryEndPoint"].Split(':');
            //IPEndPoint EndPoint = new IPEndPoint(IPAddress.Parse(ip_port.First()), Convert.ToInt32(ip_port.Last()));
            //byte[] bytes = new byte[0];

            //// Create an Identity
            //LoginRequest loginRequest = new LoginRequest()
            //{
            //    Identity = new IdentityInfo()
            //    {
            //        FirstName = configs["FirstName"],
            //        LastName = configs["LastName"],
            //        ANumber = configs["ANumber"],
            //        Alias = configs["Alias"]
            //    },
            //    ProcessLabel = configs["Alias"],
            //    ProcessType = ProcessInfo.ProcessType.Player
            //};

            //// Login Request
            //bytes = loginRequest.Encode();
            ////client.Connect(EndPoint);
            //client.Send(bytes, bytes.Length, EndPoint);

            //IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 0);

            //// Login Reply
            //client.Client.ReceiveTimeout = 30000;
            //try { bytes = client.Receive(ref ipep); }
            //catch { Console.WriteLine("Registry Connection Failure"); }
            //LoginReply loginReply = Message.Decode(bytes) as LoginReply;

            //PublicEndPoint managerEndPoint = loginReply != null ? loginReply.ProcessInfo.EndPoint : null;

            //// Request a List of Games that haven't started yet.
            //GameListRequest gameRequest = new GameListRequest() { StatusFilter = (int)GameInfo.StatusCode.Available };
            //bytes = gameRequest.Encode();
            //client.Send(bytes, bytes.Length, EndPoint);

            //// Game List Reply
            //try { bytes = client.Receive(ref EndPoint); }
            //catch { Console.WriteLine("Game List Retreival Failure"); }
            //GameListReply gameListReply = Message.Decode(bytes) as GameListReply;

            //// Request to join the first Game with the Available status
            //JoinGameRequest joinRequest = new JoinGameRequest() { Player = loginReply.ProcessInfo, GameId = gameListReply.GameInfo.First().GameId };
            //bytes = joinRequest.Encode();
            //client.Send(bytes, bytes.Length, EndPoint);

            //// Join Reply
            //try { bytes = client.Receive(ref EndPoint); }
            //catch { Console.WriteLine("Join Game Failure"); }
            Environment.Exit(1);
            //if (Encoding.Default.GetString(bytes).Contains("AliveRequest"))
            //{

            //}
            //JoinGameReply joinReply = Message.Decode(bytes) as JoinGameReply;

            //// Logout Request
            //LogoutRequest logoutReq = new LogoutRequest();
            //bytes = logoutReq.Encode();
            //client.Send(bytes, bytes.Length, EndPoint);

            //// Logout Response
            //try { bytes = client.Receive(ref EndPoint); }
            //catch { Console.WriteLine("Logout Failure"); }

            //Console.WriteLine(Encoding.Default.GetString(bytes));
            //Console.ReadLine();
            #endregion
        }
    }
}
