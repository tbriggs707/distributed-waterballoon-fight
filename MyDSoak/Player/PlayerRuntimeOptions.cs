﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    class PlayerRuntimeOptions : RuntimeOptions
    {
        public override void SetDefaults()
        {
            Registry = "52.3.213.61:12000";
            ANumber = "A01131750";
            FirstName = "Teran";
            LastName = "Briggs";
            Alias = "T-Rantula";
            Label = "T-Briggs";
            MinPortNullable = 12000;
            MaxPortNullable = 12999;
            TimeoutNullable = 3000;
            AutoStart = false;
            RetriesNullable = 3;
        }
    }
}
