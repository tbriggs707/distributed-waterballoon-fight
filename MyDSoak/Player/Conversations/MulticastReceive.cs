﻿using System;
using SharedObjects;

using Messages;
using log4net;
using CommSub;

namespace Player.Conversations
{
    public abstract class MulticastReceive : Conversation
    {
        #region Private Data Members
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MulticastReceive));

        private Routing _routingMessage;
        private Message _request;
        private int _fromProcessId;
        #endregion

        protected Message Request { get { return _request; } }
        protected int FromProcessId { get { return _fromProcessId; } }

        /// <summary>
        /// Execute the responder side of a request-reply conversation
        /// 
        /// Note that this is a temple method, four override parts:
        ///     - AllowTypes
        ///     - RemoteProcessMustBeKnown
        ///     - RemoteProcessMustBeInGame
        ///     - IsConverstationStateValid
        ///     - IsProcessStateValid
        ///     - CreateReply
        /// </summary>
        /// <param name="context"></param>
        public override void Execute(object context = null)
        {
            Logger.DebugFormat("Start {0} Conversation", GetType().Name);
            Done = false;

            if (PreExecuteAction != null)
                PreExecuteAction(context);

            if (IsEnvelopeValid(IncomingEnvelope, AllowedTypes))
            {
                if (!IsConversationStateValid())
                    Error = Error.Get(Error.StandardErrorNumbers.InvalidConversationSetup);
                else if (!IsProcessStateValid())
                    Error = Error.Get(Error.StandardErrorNumbers.InvalidProcessStateForConversation);
                else
                {
                    _routingMessage = IncomingEnvelope.Message as Routing;
                    if (_routingMessage!=null)
                    {
                        _request = _routingMessage.InnerMessage;
                        _fromProcessId = _routingMessage.FromProcessId;
                    }
                    else
                    {
                        _request = IncomingEnvelope.Message;
                        _fromProcessId = _request.MsgId.Pid;
                    }

                    Logger.DebugFormat("Reply to {0} message from {1}", _request.GetType().Name, _fromProcessId);

                    HandleRequest(Request);
                }
            }

            if (Error != null)
            {
                Logger.Warn(Error.Message);
                Process.ErrorHistory.Add(Error);
            }

            Done = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);

            Logger.DebugFormat("End {0}", GetType().Name);
        }

        /// <summary>
        /// This method returns an array of types of valid requests.  The concrete conversation must implement this method.
        /// </summary>
        protected abstract Type[] AllowedTypes { get; }

        /// <summary>
        /// This method should return true when the conversation object contains the necessary information to execute.
        /// Override this method in concrete specializations, when necessary.
        /// </summary>
        /// <returns>true is the conversation contains all of the necessary information</returns>
        protected virtual bool IsConversationStateValid() { return true; }

        /// <summary>
        /// This method should return true when the process is in a valid state for this conversation to execute.
        /// Override this method in concrete specializations, when necessary.
        /// </summary>
        /// <returns>true if the process is in a state where it is okay for this conversation to execute</returns>
        protected virtual bool IsProcessStateValid() { return true; }

        /// <summary>
        /// This method creates a reply to the request.  It must be implemented in the concrete conversation
        /// </summary>
        /// <returns></returns>
        protected abstract void HandleRequest(Message request);
    }
}
