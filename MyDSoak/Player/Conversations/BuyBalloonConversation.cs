﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using SharedObjects;
using log4net;

namespace Player.Conversations
{
    class BuyBalloonConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(BuyBalloonConversation));
        public Penny Penny { get; set; }

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(BalloonReply) };
            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("Buy Balloon Request has been generated");
            return new BuyBalloonRequest() { Penny = Penny };
        }

        protected override void ProcessReply(Reply reply)
        {
            BalloonReply bReply = reply as BalloonReply;

            Player player = Process as Player;
            player.EmptyBalloons.AddOrUpdate(bReply.Balloon);

            string msg = reply.Success ? " Buy Balloon Successful, ID: " + bReply.Balloon.Id : " Buy Balloon Failed";
            Logger.Debug("Game: " + player.CurrentGame.GameId + msg);
            //Console.WriteLine(msg);

            // mark the penny as used
        }

        protected override void PostFailureAction()
        {
            Logger.Error("BUY BALLOON FAILURE");
            // unreserve the penny
        }
    }
}
