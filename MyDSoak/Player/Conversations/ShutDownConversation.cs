﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.RequestMessages;

namespace Player.Conversations
{
    class ShutDownConversation : MulticastReceive
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(ShutdownRequest) };
            }
        }

        protected override void HandleRequest(Message request)
        {
            var player = Process as Player;
            player.Live = false;
            Environment.Exit(0);
            player.TryLogout();
            player.Stop();
        }
    }
}
