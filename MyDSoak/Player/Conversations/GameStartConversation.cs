﻿using CommSub.Conversations.ResponderConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;

namespace Player.Conversations
{
    class GameStartConversation : RequestReply
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(ReadyToStart) };
            }
        }

        protected override Message CreateReply()
        {
            (Process as Player).GameStarted = true;
            return new Reply() { Success = true };
        }
    }
}
