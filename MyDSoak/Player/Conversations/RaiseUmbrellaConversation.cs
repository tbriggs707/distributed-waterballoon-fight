﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;

namespace Player.Conversations
{
    class RaiseUmbrellaConversation : RequestReply
    {
        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(Reply) };
            }
        }

        private Player player
        {
            get
            {
                return Process as Player;
            }
        }

        protected override Message CreateRequest()
        {
            return new RaiseUmbrellaRequest()
            {
                Umbrella = player.MyUmb
            };
        }

        protected override void ProcessReply(Reply reply)
        {
            player.MyUmb = null;
        }
    }
}
