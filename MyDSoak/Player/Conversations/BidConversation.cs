﻿using CommSub.Conversations.ResponderConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using SharedObjects;

namespace Player.Conversations
{
    class BidConversation : RequestReply
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(AuctionAnnouncement) };
            }
        }

        private Player player
        {
            get
            {
                return Process as Player;
            }
        }

        protected override Message CreateReply()
        {
            Bid bid = new Bid() { Success = true };
            int bidAmount = player.InitialPennyCount / 15;

            if (player.Pennies.AvailableCount > bidAmount && player.MyUmb == null)
                bid.Pennies = player.Pennies.ReserveMany(bidAmount);
         
            return bid;
        }
    }
}
