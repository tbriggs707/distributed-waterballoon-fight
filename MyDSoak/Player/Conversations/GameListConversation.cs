﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using log4net;

namespace Player.Conversations
{
    class GameListConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(GameListConversation));

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(GameListReply) };
            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("Game List Requst has been generated");
            return new GameListRequest()
            {
                StatusFilter = (int)GameInfo.StatusCode.Available
            };
        }

        protected override void ProcessReply(Reply reply)
        {
            GameListReply listReply = reply as GameListReply;
            var player = Process as Player;
            player.Games = listReply.GameInfo;

            player.GameManagerFound = listReply != null && listReply.GameInfo != null && listReply.GameInfo.Count() > 0 ? true : false;

            string msg = reply.Success ? "Game List Successful" : "Game List Failed";
            msg += listReply.GameInfo != null ? ", Games Found: " + listReply.GameInfo.Count() : string.Empty;
            Logger.Debug(msg);
            //Console.WriteLine(msg);
        }

        protected override void PostFailureAction()
        {
            Player player = Process as Player;
            string msg = "Game IDs: null";
            if(player.Games != null)
            {
                msg = msg.Replace("null", "");
                foreach(var game in player.Games)
                    msg += game.GameId + " ";
            }

            Logger.Error("GAME LIST FAILURE: " + msg);
        }
    }
}
