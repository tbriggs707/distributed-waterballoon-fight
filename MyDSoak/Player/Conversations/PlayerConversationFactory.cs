﻿using CommSub;
using CommSub.Conversations.InitiatorConversations;
using log4net;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player.Conversations
{
    class PlayerConversationFactory : ConversationFactory
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PlayerConversationFactory));
        public override void Initialize()
        {
            // When I get an AliveRequest, start an AliveConversation
            Add(typeof(AliveRequest), typeof(AliveConversation));
            Add(typeof(AllowanceDeliveryRequest), typeof(AllowanceDistrobutionConversation));
            Add(typeof(ReadyToStart), typeof(GameStartConversation));
            Add(typeof(GameStatusNotification), typeof(GameStatusConversation));
            Add(typeof(HitNotification), typeof(HitByBalloonConversation));
            Add(typeof(ShutdownRequest), typeof(ShutDownConversation));
            Add(typeof(AuctionAnnouncement), typeof(BidConversation));
            Add(typeof(BidAck), typeof(CloseAuctionConversation));
        }
    }
}
