﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;

namespace Player.Conversations
{
    class CloseAuctionConversation : MulticastReceive
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(BidAck) };
            }
        }

        private Player player
        {
            get
            {
                return Process as Player;
            }
        }

        protected override void HandleRequest(Message request)
        {
            var bidAck = request as BidAck;

            if (bidAck.Won)
            {
                player.MyUmb = bidAck.Umbrella;
                player.TryRaiseUmbrella();
            }
        }
    }
}
