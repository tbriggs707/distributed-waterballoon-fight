﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using log4net;

namespace Player.Conversations
{
    class JoinGameConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(JoinGameConversation));
        public int JoinGameID { get; set; }

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(JoinGameReply) };
            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("Join Game Request has been generated");
            (Process as Player).GameStarted = false;
            return new JoinGameRequest()
            {
                Process = this.CommSubsystem.ParentProcess.MyProcessInfo,
                GameId = JoinGameID
            };
        }

        protected override void ProcessReply(Reply reply)
        {
            var jReply = reply as JoinGameReply;
            var player = Process as Player;
            player.LifePoints = jReply.InitialLifePoints;
            player.GamesFound = jReply != null && jReply.Success ? true : false;

            string msg = reply.Success ? "Join Game Successful, GameID: " + jReply.GameId : "Join Game Failed";
            Logger.Debug(msg);
            //Console.WriteLine(msg);
        }

        protected override void PostFailureAction()
        {
            Player player = Process as Player;
            Logger.Error("JOIN GAME FAILURE: Life points: " + player.LifePoints);
        }
    }
}
