﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using log4net;

namespace Player.Conversations
{
    class ThrowBalloonConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ThrowBalloonConversation));

        public Balloon Balloon { get; set; }
        public int TargetPlayaID { get; set; }

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(Reply) };
            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("Throw Balloon Request has been generated BalloonID: " + Balloon.Id);
            return new ThrowBalloonRequest()
            {
                Balloon = Balloon,
                TargetPlayerId = TargetPlayaID
            };
        }

        protected override void ProcessReply(Reply reply)
        {
            Player player = Process as Player;
            // Invalidate balloon
            string msg = reply.Success ? " Throw Balloon Successful" : " Throw Balloon Failure";
            Logger.Debug("Game: " + player.CurrentGame.GameId + msg);
            //Console.WriteLine(msg);
        }
    }
}
