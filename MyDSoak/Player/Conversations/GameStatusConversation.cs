﻿using Messages;
using Messages.RequestMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player.Conversations
{
    class GameStatusConversation : MulticastReceive
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(GameStatusNotification) };
            }
        }

        protected override void HandleRequest(Message request)
        {
            Player player = Process as Player;
            var notification = request as GameStatusNotification;
            player.CurrentGame = notification.Game;
        }
    }
}
