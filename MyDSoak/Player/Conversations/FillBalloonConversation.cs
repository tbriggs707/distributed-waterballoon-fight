﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using SharedObjects;
using Messages.RequestMessages;
using log4net;

namespace Player.Conversations
{
    class FillBalloonConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FillBalloonConversation));

        public Balloon Balloon { get; set; }
        public Penny[] Pennies { get; set; }

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(BalloonReply) };
            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("Balloon Fill Request generated for BalloonID: " + Balloon.Id);
            return new FillBalloonRequest()
            {
                Balloon = Balloon,
                Pennies = Pennies
            };
        }

        protected override void ProcessReply(Reply reply)
        {
            BalloonReply bReply = reply as BalloonReply;

            Player player = Process as Player;
            player.FilledBalloons.AddOrUpdate(bReply.Balloon);

            string msg = reply.Success ? " Fill Balloon Request Successful, BalloonID: " + bReply.Balloon.Id : " Fill Balloon Request Failed";
            Logger.Debug("Game: " + player.CurrentGame.GameId + msg);
            //Console.WriteLine(msg);
        }

        protected override void PostFailureAction()
        {
            Logger.Error("FILL BALLOON FAILURE");
        }
    }
}
