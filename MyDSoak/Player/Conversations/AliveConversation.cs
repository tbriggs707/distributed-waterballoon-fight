﻿using CommSub.Conversations.ResponderConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;

namespace Player.Conversations
{
    class AliveConversation : RequestReply
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(AliveRequest) };
            }
        }

        protected override Message CreateReply()
        {
            var reply = new Reply() { Success = true };
            return reply;
        }
    }
}
