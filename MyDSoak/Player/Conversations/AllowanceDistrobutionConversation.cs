﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using System.Net.Sockets;
using System.Net;
using CommSub.Conversations.ResponderConversations;
using SharedObjects;
using CommSub;

namespace Player.Conversations
{
    class AllowanceDistrobutionConversation : RequestReply
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(AllowanceDeliveryRequest) };
            }
        }

        protected override Message CreateReply()
        {
            Player player = Process as Player;

            PostExecuteAction += (context) =>
            {
                TcpClient client = new TcpClient();
                var req = Request as AllowanceDeliveryRequest;
                var bankEP = CommSubsystem.ParentProcess.PennyBankEndPoint;
                client.Connect(new IPEndPoint(bankEP.IPEndPoint.Address, req.PortNumber));

                NetworkStream clientStream = client.GetStream();
                clientStream.ReadTimeout = 1000;
                player.InitialPennyCount = req.NumberOfPennies;

                for (int i = 0; i < req.NumberOfPennies; ++i)
                {
                    Penny penny = clientStream.ReadStreamMessage();

                    if (penny != null)
                        player.Pennies.AddOrUpdate(penny);
                }
                client.Client.Close();
            };
            return new Reply() { Success = true };
        }
    }
}
