﻿using CommSub.Conversations.ResponderConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;

namespace Player.Conversations
{
    class HitByBalloonConversation : RequestReply
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(HitNotification) };
            }
        }

        protected override Message CreateReply()
        {
            var player = Process as Player;
            player.LifePoints--;

            return new Reply() { Success = true };
        }
    }
}
