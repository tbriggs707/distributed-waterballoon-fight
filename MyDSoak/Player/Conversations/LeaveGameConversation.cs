﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using log4net;

namespace Player.Conversations
{
    class LeaveGameConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(LeaveGameConversation));

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(Reply) };

            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("Leave Game Request has been generated");
            (Process as Player).GameStarted = false;
            return new LeaveGameRequest();
        }

        protected override void ProcessReply(Reply reply)
        {
            Logger.Debug("Leave Game Reply has been received");
            base.ProcessReply(reply);
        }
    }
}
