﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using log4net;

namespace Player.Conversations
{
    class LogoutConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(LogoutConversation));

        public bool ReceivedReply { get; set; }
        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(Reply) };
            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("Logout Request has been generated");
            return new LogoutRequest();
        }

        protected override void ProcessReply(Reply reply)
        {
            ReceivedReply = true;
        }

        protected override void PostFailureAction()
        {
            Logger.Error("LOGOUT FAILURE");
        }
    }
}
