﻿using CommSub;
using CommSub.Conversations.InitiatorConversations;
using log4net;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player.Conversations
{
    class LoginConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(LoginConversation));
        private IdentityInfo MyIdentity;
        private PublicEndPoint RegistryEP;

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(LoginReply) };
            }
        }

        protected override Message CreateRequest()
        {
            try
            {
                InitIdentity();
                var request = new LoginRequest()
                {
                    Identity = MyIdentity,
                    ProcessLabel = MyIdentity.Alias,
                    ProcessType = ProcessInfo.ProcessType.Player
                };
                Logger.Debug("Login Request has been created");

                return request;
            }
            catch (Exception error)
            {
                Logger.Error("Login Request Failed " + error);
                PostExecuteAction += (context) => PostFailureAction();
                return null;
            }
        }

        protected override void ProcessReply(Reply reply)
        {
            LoginReply loginReply = reply as LoginReply;
            Player player = Process as Player;

            if (loginReply != null)
            {
                player.MyProcessInfo.ProcessId = loginReply.ProcessInfo.ProcessId;
                player.PennyBankEndPoint = loginReply.PennyBankEndPoint;
                player.ProxyEndPoint = loginReply.ProxyEndPoint;
                MessageNumber.LocalProcessId = loginReply.ProcessInfo.ProcessId;

                string msg = reply.Success ? "Login Sucessful" : "Login Failed";
                msg += ": " + loginReply.ProcessInfo.ProcessId;
                Logger.Debug(msg);
                Console.WriteLine(msg);
            }
            else
                Console.WriteLine("Login Failed");
        }

        protected override void PostFailureAction()
        {
            Player player = Process as Player;

            string msg = "ProcessID: " + (player.MyProcessInfo != null ? player.MyProcessInfo.ProcessId.ToString() : "null") +
                    " PennyBankEP: " + (player.PennyBankEndPoint != null ? player.PennyBankEndPoint.HostAndPort : "null") +
                    " ProxyEP: " + (player.ProxyEndPoint != null ? player.ProxyEndPoint.HostAndPort : "null") +
                    " Local Process ID: " + MessageNumber.LocalProcessId;

            Logger.Error("LOGIN FAILURE: " + msg);
        }

        private void InitIdentity()
        {
            if (MyIdentity == null)
            {
                var configs = ConfigurationManager.AppSettings;
                RegistryEP = new PublicEndPoint(configs["RegistryEndPoint"]);

                MyIdentity = new IdentityInfo()
                {
                    FirstName = configs["FirstName"],
                    LastName = configs["LastName"],
                    ANumber = configs["ANumber"],
                    Alias = configs["Alias"]
                };
            }
        }
    }
}
