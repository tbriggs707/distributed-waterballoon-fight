﻿using CommSub.Conversations.ResponderConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;

namespace Player.Conversations
{
    class LowerUmbrellaConversation : RequestReply
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(UmbrellaLoweredNotification) };
            }
        }

        protected override Message CreateReply()
        {
            var player = Process as Player;
            player.MyUmb = null;

            return new Reply() { Success = true };
        }
    }
}
