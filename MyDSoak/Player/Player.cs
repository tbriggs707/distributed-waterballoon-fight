﻿using log4net;
using SharedObjects;
using System;
using System.Configuration;
using System.Linq;
using CommSub;
using Player.Conversations;
using Utils;

namespace Player
{
    public class Player : CommProcess
    {
        #region Private Variables
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PlayerConversationFactory));
        #endregion
        #region Public Variables
        public PublicEndPoint RegistryEP { get; set; }
        public IdentityInfo MyIdentity { get; set; }

        public GameInfo[] Games { get; set; }
        public GameInfo CurrentGame { get; set; }

        public int AssignedProcessId { get; set; }
        public int LifePoints { get; set; }
        public int InitialPennyCount { get; set; }

        public ResourceSet<Penny> Pennies { get; set; }
        public ResourceSet<Balloon> EmptyBalloons { get; set; }
        public ResourceSet<Balloon> FilledBalloons { get; set; }
        public Umbrella MyUmb { get; set; }
        public bool Live { get; set; }
        public bool GameStarted { get; set; }
        public bool GameManagerFound { get; set; }
        public bool GamesFound { get; set; }
        #endregion

        /// <summary>
        /// This is the main Game loop, this controls what happens when.
        /// </summary>
        /// <param name="state"></param>
        protected override void Process(object state)
        {
            TryLogin();
            while (Live)
            {
                TryGameList();
                if (GameManagerFound)
                {
                    TryJoinGame();
                    if (GamesFound)
                    {
                        SyncUtils.WaitForCondition(() => CurrentGame != null && CurrentGame.Status == GameInfo.StatusCode.InProgress, 30000);

                        while (KeepGoing && CurrentGame != null && CurrentGame.Status == GameInfo.StatusCode.InProgress && LifePoints > 0 && Games.Count() > 0)
                        {
                            if (Pennies.AvailableCount > 0)
                                TryBalloonBuy();

                            if (EmptyBalloons.AvailableCount > 0 && Pennies.AvailableCount > 1)
                                TryFillBalloon();

                            if (FilledBalloons.AvailableCount > 0 && GameStarted)
                                TryThrowBalloon();
                        }

                        if (LifePoints < 1)
                            TryLeaveGame();
                    }
                }
            }
            Stop();
        }

        #region Try Methods
        public void TryLogin()
        {
            var loginConvo = CommSubsystem.ConversationFactory.CreateFromConversationType<LoginConversation>();
            loginConvo.ToProcessId = 0;
            loginConvo.TargetEndPoint = new PublicEndPoint(Options.Registry);
            loginConvo.Execute();
        }

        public void TryGameList()
        {
            var gameListConvo = CommSubsystem.ConversationFactory.CreateFromConversationType<GameListConversation>();
            gameListConvo.ToProcessId = 1;
            gameListConvo.TargetEndPoint = new PublicEndPoint(Options.Registry);
            gameListConvo.Execute();
        }

        public void TryJoinGame()
        {
            if (Games.Count() < 1 || (CurrentGame != null && CurrentGame.Status == GameInfo.StatusCode.InProgress)) return;

            var joinGameConvo = CommSubsystem.ConversationFactory.CreateFromConversationType<JoinGameConversation>();
            CurrentGame = Games.First(g => g.Status == GameInfo.StatusCode.Available);
            joinGameConvo.JoinGameID = CurrentGame.GameId;
            joinGameConvo.ToProcessId = CurrentGame.GameManagerId;
            joinGameConvo.Execute();
        }

        public void TryBalloonBuy()
        {
            if (MyUmb != null)
                TryRaiseUmbrella();

            //SyncUtils.WaitForCondition(() => CurrentGame.Status == GameInfo.StatusCode.InProgress, 20000);
            Penny pennyToBuy = null;
            while (pennyToBuy == null)
                pennyToBuy = Pennies.ReserveOne();

            var buyBalloonConvo = CommSubsystem.ConversationFactory.CreateFromConversationType<BuyBalloonConversation>();
            buyBalloonConvo.ToProcessId = CurrentGame.CurrentProcesses.First(p => p.Type == ProcessInfo.ProcessType.BalloonStore).ProcessId;
            buyBalloonConvo.Penny = pennyToBuy;
            buyBalloonConvo.Execute();
        }

        public void TryFillBalloon()
        {
            if (MyUmb != null)
                TryRaiseUmbrella();

            Penny[] penniesToFill = null;
            while (penniesToFill == null)
                penniesToFill = Pennies.ReserveMany(2);

            var fillBalloonConvo = CommSubsystem.ConversationFactory.CreateFromConversationType<FillBalloonConversation>();
            fillBalloonConvo.ToProcessId = CurrentGame.CurrentProcesses.First(p => p.Type == ProcessInfo.ProcessType.WaterServer).ProcessId;
            fillBalloonConvo.Balloon = EmptyBalloons.GetAvailable();
            fillBalloonConvo.Pennies = penniesToFill;
            fillBalloonConvo.Execute();
        }

        public void TryThrowBalloon()
        {
            if (MyUmb != null)
                TryRaiseUmbrella();

            try
            {
                var throwBalloonConvo = CommSubsystem.ConversationFactory.CreateFromConversationType<ThrowBalloonConversation>();
                throwBalloonConvo.Balloon = FilledBalloons.GetAvailable();
                throwBalloonConvo.TargetPlayaID = CurrentGame.CurrentProcesses.Where(p => p.Type == ProcessInfo.ProcessType.Player).Last(p => p.ProcessId != MyProcessInfo.ProcessId).ProcessId;
                throwBalloonConvo.ToProcessId = CurrentGame.GameManagerId;
                throwBalloonConvo.Execute();
            }
            catch (Exception exception)
            {
                Logger.Error("Throw Balloon Failed, possibly there are no players to throw at.");
                Logger.Error(exception.Message);
            }
        }

        public void TryRaiseUmbrella()
        {
            try
            {
                if (MyUmb != null)
                {
                    var convo = CommSubsystem.ConversationFactory.CreateFromConversationType<RaiseUmbrellaConversation>();
                    convo.ToProcessId = CurrentGame.GameManagerId;
                    convo.Execute();
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Raise Umbrella Failed");
                Logger.Error(exception.Message);
            }
        }

        public void TryLeaveGame()
        {
            if (CurrentGame != null)
            {
                var leaveGameConvo = CommSubsystem.ConversationFactory.CreateFromConversationType<LeaveGameConversation>();
                leaveGameConvo.ToProcessId = CurrentGame.GameManagerId;
                leaveGameConvo.Execute();
                CurrentGame = null;
                GameManagerFound = false;
                GamesFound = false;
            }
        }

        public void TryLogout()
        {
            TryLeaveGame();

            Live = false;
            var logoutConvo = CommSubsystem.ConversationFactory.CreateFromConversationType<LogoutConversation>();
            logoutConvo.ToProcessId = 1;
            logoutConvo.TargetEndPoint = new PublicEndPoint(Options.Registry);
            logoutConvo.Execute();
            SyncUtils.WaitForCondition(() => logoutConvo.ReceivedReply, 5000);
        }
        #endregion Try Methods

        #region Other Methods
        public void Initialize()
        {
            Logger.Debug("Begin Initialization");

            Live = true;


            var configs = ConfigurationManager.AppSettings;
            RegistryEP = new PublicEndPoint(configs["RegistryEndPoint"]);

            MyIdentity = new IdentityInfo()
            {
                FirstName = configs["FirstName"],
                LastName = configs["LastName"],
                ANumber = configs["ANumber"],
                Alias = configs["Alias"]
            };

            MyUmb = null;
            Pennies = new ResourceSet<Penny>();
            EmptyBalloons = new ResourceSet<Balloon>();
            FilledBalloons = new ResourceSet<Balloon>();
            Options = new PlayerRuntimeOptions();
            Options.SetDefaults();

            MyProcessInfo = new ProcessInfo
            {
                ProcessId = AssignedProcessId,
                Label = Options.Label,
                Type = ProcessInfo.ProcessType.Player,
                Status = ProcessInfo.StatusCode.NotInitialized
            };

            PlayerConversationFactory conversationFactory = new PlayerConversationFactory()
            {
                Process = this,
                DefaultMaxRetries = Options.Retries,
                DefaultTimeout = Options.Timeout
            };
            SetupCommSubsystem(conversationFactory);

            MyProcessInfo.EndPoint = new PublicEndPoint() { Host = "127.0.0.1", Port = MyCommunicator.Port };
            Logger.DebugFormat("Process {0}'s End Point = {1}", MyProcessInfo.ProcessId, MyProcessInfo.EndPoint);
            Logger.Debug("End Initialization");
        }
        public override void Start()
        {
            Logger.Debug("Enter Start");

            Initialize();

            base.Start();
        }
        public override void CleanupSession()
        {
            base.CleanupSession();
        }
        protected override void CleanupProcess()
        {
            base.CleanupProcess();
        }
        public override void Stop()
        {
            TryLogout();
            base.Stop();
            Environment.Exit(0);
        }
        #endregion Other Methods
    }
}
