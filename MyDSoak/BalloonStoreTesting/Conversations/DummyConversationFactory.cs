﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSub;
using Messages.RequestMessages;
using BalloonStoreTesting.Conversations.Dummies;

namespace BalloonStoreTesting.Conversations
{
    [TestClass]
    public class DummyConversationFactory : ConversationFactory
    {
        public override void Initialize()
        {
            Add(typeof(AliveRequest), typeof(DummyAliveConversation));
        }
    }
}
