﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStoreTesting.Conversations.Dummies
{
    class DummyBuyBalloonConversation : Conversation
    {
        private static readonly List<DummyBuyBalloonConversation> Instances = new List<DummyBuyBalloonConversation>();
        public static List<DummyBuyBalloonConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyBuyBalloonConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyBuyBalloonConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
