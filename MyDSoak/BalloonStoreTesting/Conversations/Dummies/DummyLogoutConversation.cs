﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStoreTesting.Conversations.Dummies
{
    class DummyLogoutConversation : Conversation
    {
        private static readonly List<DummyLogoutConversation> Instances = new List<DummyLogoutConversation>();
        public static List<DummyLogoutConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyLogoutConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyLogoutConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
