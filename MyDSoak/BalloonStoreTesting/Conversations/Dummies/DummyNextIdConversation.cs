﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStoreTesting.Conversations.Dummies
{
    class DummyNextIdConversation : Conversation
    {
        private static readonly List<DummyNextIdConversation> Instances = new List<DummyNextIdConversation>();
        public static List<DummyNextIdConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyNextIdConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyNextIdConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
