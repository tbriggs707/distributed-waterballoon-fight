﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStoreTesting.Conversations.Dummies
{
    class DummyLoginConversation : Conversation
    {
        private static readonly List<DummyLoginConversation> Instances = new List<DummyLoginConversation>();
        public static List<DummyLoginConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyLoginConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyLoginConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
