﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStoreTesting.Conversations.Dummies
{
    class DummyLeaveGameConversation : Conversation
    {
        private static readonly List<DummyLeaveGameConversation> Instances = new List<DummyLeaveGameConversation>();
        public static List<DummyLeaveGameConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyLeaveGameConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyLeaveGameConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
