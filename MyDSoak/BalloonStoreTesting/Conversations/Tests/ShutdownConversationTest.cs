﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSub;
using System.Threading;
using Messages.RequestMessages;

namespace BalloonStoreTesting.Conversations.Tests
{
    [TestClass]
    public class ShutdownConversationTest : ConversationTest
    {
        [TestMethod]
        public void BalloonStore_ShutdownTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Request
            Request = new ShutdownRequest()
            {
                MsgId = MessageNum,
                ConvId = MessageNum
            };

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
