﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSub;
using System.Threading;
using BalloonStoreTesting.Conversations.Tests;

namespace BalloonStore.Conversations.Tests
{
    [TestClass]
    public class GameStatusConversationTest : ConversationTest
    {
        [TestMethod]
        public void BalloonStore_GameStatusTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
