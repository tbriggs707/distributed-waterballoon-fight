﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSubTesting;
using SharedObjects;
using CommSub;
using BalloonStoreTesting.Conversations.Dummies;
using Messages.RequestMessages;
using System.Threading;

namespace BalloonStoreTesting.Conversations.Tests
{
    [TestClass]
    public abstract class ConversationTest
    {
        public BalloonStore.BalloonStore Store { get; set; }
        public Conversation Convo { get; set; }
        public CommSubsystem CommSubSystem { get; set; }
        public Communicator RemoteComm { get; set; }
        protected PublicEndPoint TargetEP { get; set; }
        protected MessageNumber MessageNum { get; set; }
        protected Request Request { get; set; }

        public void RunInitialTests(Type type)
        {
            Store = new BalloonStore.BalloonStore();

            // Setup a fake process id
            MessageNumber.LocalProcessId = 10;

            // Create a commSubsystem with a Listener
            CommSubSystem = TestUtilities.SetupTestCommSubsystem(new DummyConversationFactory());

            Convo = GetConversation(type);

            // Get the EndPoint for the commSubsystem, so we can send messages to it
            TargetEP = new PublicEndPoint()
            {
                Host = "127.0.0.1",
                Port = CommSubSystem.Communicator.Port
            };

            MessageNum = MessageNumber.Create();

            // Create another Communicator to send stuff to the listener.
            RemoteComm = new Communicator() { MinPort = 12000, MaxPort = 12099 };
            RemoteComm.Start();

            // Check initial state
            int createdInstancesCount = GetCreatedInstancesCount(type);
            Assert.AreEqual(0, CommSubSystem.QueueDictionary.ConversationQueueCount);
            Assert.AreEqual(1, createdInstancesCount);

            Thread.Sleep(3000);
        }

        public void RunPostTests(Type type)
        {
            int createdInstancesCount = GetCreatedInstancesCount(type);
            bool executed = GetExecuteCall(type);

            Assert.AreEqual(1, createdInstancesCount);
            Assert.IsTrue(executed);
        }

        private bool GetExecuteCall(Type type)
        {
            #region Switch
            switch (type.Name)
            {
                case "BuyBalloonConversationTest":
                    return DummyBuyBalloonConversation.LastInstance.ExecuteCalled;

                case "GameStartConversationTest":
                    return DummyGameStartConversation.LastInstance.ExecuteCalled;

                case "GameStatusConversationTest":
                    return DummyGameStatusConversation.LastInstance.ExecuteCalled;
                    
                case "JoinGameConversationTest":
                    return DummyJoinGameConversation.LastInstance.ExecuteCalled;

                case "LeaveGameConversationTest":
                    return DummyLeaveGameConversation.LastInstance.ExecuteCalled;

                case "LoginConversationTest":
                    return DummyLoginConversation.LastInstance.ExecuteCalled;

                case "LogoutConversationTest":
                    return DummyLogoutConversation.LastInstance.ExecuteCalled;

                case "ShutdownConversationTest":
                    return DummyShutdownConversation.LastInstance.ExecuteCalled;

                case "NextIdConversationTest":
                    return DummyNextIdConversation.LastInstance.ExecuteCalled;

                default:
                    return false;
            }
            #endregion Switch
        }

        private int GetCreatedInstancesCount(Type type)
        {
            #region Switch
            switch (type.Name)
            {
                case "BuyBalloonConversationTest":
                    return DummyBuyBalloonConversation.CreatedInstances.Count;

                case "GameStartConversationTest":
                    return DummyGameStartConversation.CreatedInstances.Count;

                case "GameStatusConversationTest":
                    return DummyGameStatusConversation.CreatedInstances.Count;

                case "JoinGameConversationTest":
                    return DummyJoinGameConversation.CreatedInstances.Count;

                case "LeaveGameConversationTest":
                    return DummyLeaveGameConversation.CreatedInstances.Count;

                case "LoginConversationTest":
                    return DummyLoginConversation.CreatedInstances.Count;

                case "LogoutConversationTest":
                    return DummyLogoutConversation.CreatedInstances.Count;

                case "ShutdownConversationTest":
                    return DummyShutdownConversation.CreatedInstances.Count;

                case "NextIdConversationTest":
                    return DummyNextIdConversation.CreatedInstances.Count;

                default:
                    return 1;
            }
            #endregion Switch
        }

        private Conversation GetConversation(Type type)
        {
            #region Switch
            switch (type.Name)
            {
                case "BuyBalloonConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyBuyBalloonConversation>();

                case "GameStartConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyGameStartConversation>();

                case "GameStatusConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyGameStatusConversation>();

                case "JoinGameConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyJoinGameConversation>();

                case "LeaveGameConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyLeaveGameConversation>();

                case "LoginConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyLoginConversation>();

                case "LogoutConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyLogoutConversation>();

                case "ShutdownConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyShutdownConversation>();

                case "NextIdConversationTest":
                    return CommSubSystem.ConversationFactory.CreateFromConversationType<DummyNextIdConversation>();

                default:
                    return null;
            }
            #endregion Switch
        }
    }
}
