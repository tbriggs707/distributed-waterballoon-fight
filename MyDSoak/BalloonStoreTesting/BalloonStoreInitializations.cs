﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BalloonStore;
using BalloonStore.Conversations;
using SharedObjects;
using CommSub;
using System.Security.Cryptography;

namespace BalloonStoreTesting
{
    [TestClass]
    public class BalloonStoreInitializations
    {
        [TestMethod]
        public void BalloonStore_Initialization()
        {
            #region Runtime Options
            BalloonStoreRuntimeOptions options = new BalloonStoreRuntimeOptions();
            options.SetDefaults();

            Assert.AreEqual("127.0.0.1:12000", options.Registry);
            Assert.AreEqual("A00003456", options.ANumber);
            Assert.AreEqual("James", options.FirstName);
            Assert.AreEqual("Tester", options.LastName);
            Assert.AreEqual("T-Rantula", options.Alias);
            Assert.AreEqual("T-Rantula", options.Label);
            Assert.AreEqual(12000, options.MinPortNullable);
            Assert.AreEqual(12999, options.MaxPortNullable);
            Assert.AreEqual(3000, options.TimeoutNullable);
            Assert.AreEqual(false, options.AutoStart);
            Assert.AreEqual(3, options.RetriesNullable);
            #endregion

            BalloonStore.BalloonStore store = new BalloonStore.BalloonStore() { Options = options };

            #region Registry EndPoint
            Assert.IsNull(store.RegistryEndPoint);
            store.RegistryEndPoint = new PublicEndPoint() { HostAndPort = store.Options.Registry };
            Assert.AreEqual("127.0.0.1:12000", store.RegistryEndPoint.ToString());
            #endregion

            #region Generate Balloon Action
            Conversation.ActionHandler generateBalloonsAction = new Conversation.ActionHandler(store.SetupBalloons);
            Assert.IsNotNull(generateBalloonsAction);
            #endregion

            #region RSA
            Assert.IsNull(store.RSA);
            store.RSA = new RSACryptoServiceProvider();
            Assert.IsNotNull(store.RSA);

            RSAParameters senderPublicKeyInfo = store.RSA.ExportParameters(true);
            Assert.IsNotNull(senderPublicKeyInfo);
            #endregion

            #region Public Key
            Assert.IsNull(store.MyPublicKey);
            store.MyPublicKey = new PublicKey
            {
                Exponent = senderPublicKeyInfo.Exponent,
                Modulus = senderPublicKeyInfo.Modulus
            };
            Assert.IsNotNull(store.MyPublicKey);
            Assert.AreEqual(store.MyPublicKey.Exponent, senderPublicKeyInfo.Exponent);
            Assert.AreEqual(store.MyPublicKey.Modulus, senderPublicKeyInfo.Modulus);
            #endregion

            #region RSA Client
            RSAClient signerValidator = new RSAClient
            {
                RegistryEndPoint = store.RegistryEndPoint,
                RSALookup = new RSAClient.RsaLookupMethod(store.KnownProcesses.GetProcessRsa),
                MySigner = new RSAPKCS1SignatureFormatter(store.RSA)
            };
            Assert.IsNotNull(signerValidator);
            Assert.AreEqual(signerValidator.RegistryEndPoint, store.RegistryEndPoint);
            Assert.IsNotNull(signerValidator.RSALookup);

            signerValidator.CommSubsystem = store.CommSubsystem;
            Assert.AreEqual(signerValidator.CommSubsystem, store.CommSubsystem);
            #endregion

            #region Identity
            Assert.IsNull(store.Identity);
            store.Identity = new IdentityInfo
            {
                ANumber = store.Options.ANumber,
                FirstName = store.Options.FirstName,
                LastName = store.Options.LastName,
                Alias = store.Options.Alias
            };
            Assert.IsNotNull(store.Identity);
            Assert.AreEqual(store.Identity.ANumber, store.Options.ANumber);
            Assert.AreEqual(store.Identity.FirstName, store.Options.FirstName);
            Assert.AreEqual(store.Identity.LastName, store.Options.LastName);
            Assert.AreEqual(store.Identity.Alias, store.Options.Alias);
            #endregion

            #region Current Game Info
            Assert.IsNull(store.CurrGameInfo);
            store.CurrGameInfo = new GameInfo
            {
                GameId = store.Options.GameId,
                GameManagerId = store.Options.GameManagerId
            };
            Assert.IsNotNull(store.CurrGameInfo);
            Assert.AreEqual(store.CurrGameInfo.GameId, store.Options.GameId);
            Assert.AreEqual(store.CurrGameInfo.GameManagerId, store.Options.GameManagerId);
            #endregion

            #region Conversation Factory
            ConversationFactory conversationFactory = new BalloonStoreConversationFactory
            {
                Process = store,
                DefaultMaxRetries = store.Options.Retries,
                DefaultTimeout = store.Options.Timeout
            };
            Assert.IsNotNull(conversationFactory);
            Assert.AreEqual(conversationFactory.Process, store);
            Assert.AreEqual(conversationFactory.DefaultMaxRetries, store.Options.Retries);
            Assert.AreEqual(conversationFactory.DefaultTimeout, store.Options.Timeout);
            #endregion

            #region Game Manager Info
            GameProcessData gameMan = new GameProcessData
            {
                ProcessId = store.Options.GameManagerId,
                Type = ProcessInfo.ProcessType.GameManager
            };
            Assert.IsNotNull(gameMan);
            Assert.AreEqual(gameMan.ProcessId, store.Options.GameManagerId);
            Assert.AreEqual(gameMan.Type, ProcessInfo.ProcessType.GameManager);
            #endregion

            #region Balloon Store Info
            Assert.IsNull(store.MyProcessInfo);
            store.MyProcessInfo = new ProcessInfo
            {
                Label = store.Options.Label,
                Type = ProcessInfo.ProcessType.BalloonStore,
                Status = ProcessInfo.StatusCode.Initializing
            };
            Assert.IsNotNull(store.MyProcessInfo);
            Assert.AreEqual(store.MyProcessInfo.Label, store.Options.Label);
            Assert.AreEqual(store.MyProcessInfo.Type, ProcessInfo.ProcessType.BalloonStore);
            Assert.AreEqual(store.MyProcessInfo.Status, ProcessInfo.StatusCode.Initializing);
            #endregion

            #region Known Processes
            Assert.IsNotNull(store.KnownProcesses.Processes.Count);
            store.KnownProcesses.AddOrUpdate(gameMan);
            #endregion

            #region CommSubsystem
            Assert.IsNull(store.CommSubsystem);
            store.SetupCommSubsystem(conversationFactory, store.Options.MinPort, store.Options.MaxPort);
            Assert.IsNotNull(store.CommSubsystem);
            Assert.IsNotNull(store.CommSubsystem.Communicator);
            Assert.IsNotNull(store.CommSubsystem.ConversationFactory);
            Assert.IsNotNull(store.CommSubsystem.Dispatcher);
            Assert.IsNotNull(store.CommSubsystem.QueueDictionary);
            Assert.AreEqual(store.CommSubsystem.QueueDictionary.ConversationQueueCount, 0);
            Assert.IsNotNull(store.CommSubsystem.MaxPort);
            Assert.IsNotNull(store.CommSubsystem.MinPort);
            Assert.IsNotNull(store.CommSubsystem.ParentProcess);
            #endregion
        }
    }
}
