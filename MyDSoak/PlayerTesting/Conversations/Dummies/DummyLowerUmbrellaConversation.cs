﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerTesting.Conversations.Dummies
{
    class DummyLowerUmbrellaConversation : Conversation
    {
        private static readonly List<DummyLowerUmbrellaConversation> Instances = new List<DummyLowerUmbrellaConversation>();
        public static List<DummyLowerUmbrellaConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyLowerUmbrellaConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyLowerUmbrellaConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
