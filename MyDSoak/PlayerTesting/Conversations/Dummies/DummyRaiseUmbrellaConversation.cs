﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerTesting.Conversations.Dummies
{
    class DummyRaiseUmbrellaConversation : Conversation
    {
        private static readonly List<DummyRaiseUmbrellaConversation> Instances = new List<DummyRaiseUmbrellaConversation>();
        public static List<DummyRaiseUmbrellaConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyRaiseUmbrellaConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyRaiseUmbrellaConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
