﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerTesting.Conversations.Dummies
{
    class DummyBidConversation : Conversation
    {
        private static readonly List<DummyBidConversation> Instances = new List<DummyBidConversation>();
        public static List<DummyBidConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyBidConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyBidConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
