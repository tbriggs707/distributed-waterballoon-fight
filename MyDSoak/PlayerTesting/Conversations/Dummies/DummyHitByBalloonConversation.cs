﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerTesting.Conversations.Dummies
{
    class DummyHitByBalloonConversation : Conversation
    {
        private static readonly List<DummyHitByBalloonConversation> Instances = new List<DummyHitByBalloonConversation>();
        public static List<DummyHitByBalloonConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyHitByBalloonConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyHitByBalloonConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
