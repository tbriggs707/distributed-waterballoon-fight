﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerTesting.Conversations.Dummies
{
    class DummyFillBalloonConversation : Conversation
    {
        private static readonly List<DummyFillBalloonConversation> Instances = new List<DummyFillBalloonConversation>();
        public static List<DummyFillBalloonConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyFillBalloonConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyFillBalloonConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
