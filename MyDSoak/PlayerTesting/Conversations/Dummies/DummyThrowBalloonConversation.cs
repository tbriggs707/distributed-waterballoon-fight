﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerTesting.Conversations.Dummies
{
    class DummyThrowBalloonConversation : Conversation
    {
        private static readonly List<DummyThrowBalloonConversation> Instances = new List<DummyThrowBalloonConversation>();
        public static List<DummyThrowBalloonConversation> CreatedInstances
        {
            get { return Instances; }
        }
        public static DummyThrowBalloonConversation LastInstance
        {
            get
            {
                return (CreatedInstances.Count != 0) ? CreatedInstances.Last() : null;
            }
        }
        public bool ExecuteCalled { get; set; }

        public DummyThrowBalloonConversation()
        {
            CreatedInstances.Add(this);
            //LastInstance.ExecuteCalled = true;
        }

        public override void Execute(object context = null)
        {
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            ExecuteCalled = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
