﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using CommSub;
using SharedObjects;
using System.Threading;
using Messages.RequestMessages;
using CommSubTesting;
using Player.Conversations;
using PlayerTesting.Conversations.Dummies;

namespace PlayerTesting.Conversations.Tests
{
    [TestClass]
    public class BuyBalloonConversationTest : ConversationTest
    {
        [TestMethod]
        public void Player_BuyBalloonTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Request
            Request = new BuyBalloonRequest()
            {
                Penny = new Penny() { Id = 401 },
                MsgId = MessageNum,
                ConvId = MessageNum
            };

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
