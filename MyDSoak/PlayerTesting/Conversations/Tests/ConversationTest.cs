﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSubTesting;
using SharedObjects;
using CommSub;
using PlayerTesting.Conversations.Dummies;
using Messages.RequestMessages;
using System.Threading;

namespace PlayerTesting.Conversations.Tests
{
    [TestClass]
    public abstract class ConversationTest
    {
        public Player.Player Player { get; set; }
        public Conversation Convo { get; set; }
        public CommSubsystem CommSubSystem { get; set; }
        public Communicator RemoteComm { get; set; }
        protected PublicEndPoint TargetEP { get; set; }
        protected MessageNumber MessageNum { get; set; }
        protected Request Request { get; set; }

        public void RunInitialTests(Type type)
        {
            Player = new Player.Player();
            Player.Initialize();
            PlayerTests.AppConfigsReplacement(Player);

            // Setup a fake process id
            MessageNumber.LocalProcessId = 10;

            // Create a commSubsystem with a Listener
            CommSubSystem = TestUtilities.SetupTestCommSubsystem(new MyDummyConversationFactory());

            #region Switch
            switch (type.Name)
            {
                case "BidConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyBidConversation>();
                    break;

                case "CloseAuctionConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyCloseAuctionConversation>();
                    break;

                case "LowerUmbrellaConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyLowerUmbrellaConversation>();
                    break;

                case "RaiseUmbrellaConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyRaiseUmbrellaConversation>();
                    break;

                case "BuyBalloonConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyBuyBalloonConversation>();
                    break;

                case "FillBalloonConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyFillBalloonConversation>();
                    break;

                case "GameListConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyGameListConversation>();
                    break;

                case "GameStartConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyGameStartConversation>();
                    break;

                case "GameStatusConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyGameStatusConversation>();
                    break;

                case "HitByBalloonConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyHitByBalloonConversation>();
                    break;

                case "JoinGameConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyJoinGameConversation>();
                    break;

                case "LeaveGameConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyLeaveGameConversation>();
                    break;

                case "LoginConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyLoginConversation>();
                    break;

                case "LogoutConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyLogoutConversation>();
                    break;

                case "ShutdownConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyShutdownConversation>();
                    break;

                case "ThrowBalloonConversationTest":
                    Convo = CommSubSystem.ConversationFactory.CreateFromConversationType<DummyThrowBalloonConversation>();
                    break;
            }
            #endregion Switch

            // Get the EndPoint for the commSubsystem, so we can send messages to it
            TargetEP = new PublicEndPoint()
            {
                Host = "127.0.0.1",
                Port = CommSubSystem.Communicator.Port
            };

            MessageNum = MessageNumber.Create();

            // Create another Communicator to send stuff to the listener.
            RemoteComm = new Communicator() { MinPort = 12000, MaxPort = 12099 };
            RemoteComm.Start();

            // Check initial state
            int createdInstancesCount = GetCreatedInstancesCount(type);
            Assert.AreEqual(0, CommSubSystem.QueueDictionary.ConversationQueueCount);
            Assert.AreEqual(1, createdInstancesCount);

            Thread.Sleep(3000);
        }

        public void RunPostTests(Type type)
        {
            int createdInstancesCount = GetCreatedInstancesCount(type);
            bool executed = GetExecuteCall(type);

            Assert.AreEqual(1, createdInstancesCount);
            Assert.IsTrue(executed);
        }

        private bool GetExecuteCall(Type type)
        {
            #region Switch
            switch (type.Name)
            {
                case "BidConversationTest":
                    return DummyBidConversation.LastInstance.ExecuteCalled;

                case "CloseAuctionConversationTest":
                    return DummyCloseAuctionConversation.LastInstance.ExecuteCalled;

                case "LowerUmbrellaConversationTest":
                    return DummyLowerUmbrellaConversation.LastInstance.ExecuteCalled;

                case "RaiseUmbrellaConversationTest":
                    return DummyRaiseUmbrellaConversation.LastInstance.ExecuteCalled;

                case "BuyBalloonConversationTest":
                    return DummyBuyBalloonConversation.LastInstance.ExecuteCalled;

                case "FillBalloonConversationTest":
                    return DummyFillBalloonConversation.LastInstance.ExecuteCalled;

                case "GameListConversationTest":
                    return DummyGameListConversation.LastInstance.ExecuteCalled;

                case "GameStartConversationTest":
                    return DummyGameStartConversation.LastInstance.ExecuteCalled;

                case "GameStatusConversationTest":
                    return DummyGameStatusConversation.LastInstance.ExecuteCalled;

                case "HitByBalloonConversationTest":
                    return DummyHitByBalloonConversation.LastInstance.ExecuteCalled;

                case "JoinGameConversationTest":
                    return DummyJoinGameConversation.LastInstance.ExecuteCalled;

                case "LeaveGameConversationTest":
                    return DummyLeaveGameConversation.LastInstance.ExecuteCalled;

                case "LoginConversationTest":
                    return DummyLoginConversation.LastInstance.ExecuteCalled;

                case "LogoutConversationTest":
                    return DummyLogoutConversation.LastInstance.ExecuteCalled;

                case "ShutdownConversationTest":
                    return DummyShutdownConversation.LastInstance.ExecuteCalled;

                case "ThrowBalloonConversationTest":
                    return DummyThrowBalloonConversation.LastInstance.ExecuteCalled;

                default:
                    return false;
            }
            #endregion Switch
        }

        private int GetCreatedInstancesCount(Type type)
        {
            #region Switch
            switch (type.Name)
            {
                case "BidConversationTest":
                    return DummyBidConversation.CreatedInstances.Count;

                case "CloseAuctionConversationTest":
                    return DummyCloseAuctionConversation.CreatedInstances.Count;

                case "LowerUmbrellaConversationTest":
                    return DummyLowerUmbrellaConversation.CreatedInstances.Count;

                case "RaiseUmbrellaConversationTest":
                    return DummyRaiseUmbrellaConversation.CreatedInstances.Count;

                case "BuyBalloonConversationTest":
                    return DummyBuyBalloonConversation.CreatedInstances.Count;

                case "FillBalloonConversationTest":
                    return DummyFillBalloonConversation.CreatedInstances.Count;

                case "GameListConversationTest":
                    return DummyGameListConversation.CreatedInstances.Count;

                case "GameStartConversationTest":
                    return DummyGameStartConversation.CreatedInstances.Count;

                case "GameStatusConversationTest":
                    return DummyGameStatusConversation.CreatedInstances.Count;

                case "HitByBalloonConversationTest":
                    return DummyHitByBalloonConversation.CreatedInstances.Count;

                case "JoinGameConversationTest":
                    return DummyJoinGameConversation.CreatedInstances.Count;

                case "LeaveGameConversationTest":
                    return DummyLeaveGameConversation.CreatedInstances.Count;

                case "LoginConversationTest":
                    return DummyLoginConversation.CreatedInstances.Count;

                case "LogoutConversationTest":
                    return DummyLogoutConversation.CreatedInstances.Count;

                case "ShutdownConversationTest":
                    return DummyShutdownConversation.CreatedInstances.Count;

                case "ThrowBalloonConversationTest":
                    return DummyThrowBalloonConversation.CreatedInstances.Count;

                default:
                    return 1;
            }
            #endregion Switch
        }
    }
}
