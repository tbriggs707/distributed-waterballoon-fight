﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages.RequestMessages;
using SharedObjects;
using CommSub;
using System.Threading;

namespace PlayerTesting.Conversations.Tests
{
    [TestClass]
    public class RaiseUmbrellaConversationTest : ConversationTest
    {
        [TestMethod]
        public void Player_RaiseUmbrellaTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Request
            Request = new RaiseUmbrellaRequest()
            {
                MsgId = MessageNum,
                ConvId = MessageNum,
                Umbrella = new Umbrella()
            };

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
