﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSub;
using System.Threading;
using Messages.RequestMessages;

namespace PlayerTesting.Conversations.Tests
{
    [TestClass]
    public class CloseAuctionConversationTest : ConversationTest
    {
        [TestMethod]
        public void Player_CloseAuctionTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
