﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSub;
using System.Threading;

namespace PlayerTesting.Conversations.Tests
{
    [TestClass]
    public class LowerUmbrellaConversationTest : ConversationTest
    {
        [TestMethod]
        public void Player_LowerUmbrellaTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
