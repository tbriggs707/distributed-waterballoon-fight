﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSub;
using System.Threading;
using Messages.RequestMessages;

namespace PlayerTesting.Conversations.Tests
{
    [TestClass]
    public class LeaveGameConversationTest : ConversationTest
    {
        [TestMethod]
        public void Player_LeaveGameTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Request
            Request = new LeaveGameRequest()
            {
                MsgId = MessageNum,
                ConvId = MessageNum
            };

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
