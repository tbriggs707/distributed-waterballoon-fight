﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSub;
using System.Threading;
using Messages.RequestMessages;

namespace PlayerTesting.Conversations.Tests
{
    [TestClass]
    public class LoginConversationTest : ConversationTest
    {
        [TestMethod]
        public void Player_LoginTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Request
            Request = new LoginRequest() { MsgId = MessageNum, ConvId = MessageNum };
            (Request as LoginRequest).Identity = Player.MyIdentity;

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
