﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using CommSub;
using SharedObjects;
using System.Threading;
using Messages.RequestMessages;
using CommSubTesting;
using Player.Conversations;
using PlayerTesting.Conversations.Dummies;

namespace PlayerTesting.Conversations.Tests
{
    [TestClass]
    public class FillBalloonConversationTest : ConversationTest
    {
        [TestMethod]
        public void Player_FillBalloonTest()
        {
            var type = this.GetType();

            RunInitialTests(type);

            // Create Request
            Penny[] pennies = new[] { new Penny() { Id = 301 }, new Penny() { Id = 302 } };
            Request = new FillBalloonRequest()
            {
                Balloon = new Balloon() { Id = 401 },
                Pennies = pennies,
                MsgId = MessageNum,
                ConvId = MessageNum
            };

            // Create Envelope, Send Envelope
            Envelope env1 = new Envelope() { Message = Request, EndPoint = TargetEP };
            RemoteComm.Send(env1);

            Thread.Sleep(3000);
            Convo.Execute();

            RunPostTests(type);
        }
    }
}
