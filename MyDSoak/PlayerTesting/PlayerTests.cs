﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Threading;
using Messages;
using Messages.RequestMessages;
using System.Net.Sockets;
using SharedObjects;
using System.Linq;
using System.Collections.Generic;

namespace PlayerTesting
{
    [TestClass]
    public class PlayerTests
    {
        [TestMethod]
        public void InitializePlayer()
        {
            Player.Player player = new Player.Player();

            player.Initialize();
            AppConfigsReplacement(player);

            // Ready to begin
            Assert.IsTrue(player.Live);

            // Identity Info
            Assert.IsNotNull(player.MyIdentity);
            Assert.AreEqual("James", player.MyIdentity.FirstName);
            Assert.AreEqual("Tester", player.MyIdentity.LastName);
            Assert.AreEqual("A00003456", player.MyIdentity.ANumber);
            Assert.AreEqual("T-Rantula", player.MyIdentity.Alias);

            // Registry End Point
            Assert.AreEqual("127.0.0.1:12000", player.RegistryEP.HostAndPort);

            // Resources
            Assert.IsNotNull(player.Pennies);
            Assert.IsNotNull(player.EmptyBalloons);
            Assert.IsNotNull(player.FilledBalloons);

            // Runtime Options
            Assert.IsNotNull(player.Options);
            Assert.AreEqual("127.0.0.1:12000", player.Options.Registry);
            Assert.AreEqual("A01131750", player.Options.ANumber);
            Assert.AreEqual("Teran", player.Options.FirstName);
            Assert.AreEqual("Briggs", player.Options.LastName);
            Assert.AreEqual("T-Rantula", player.Options.Alias);
            Assert.AreEqual("T-Briggs", player.Options.Label);
            Assert.AreEqual(12000, player.Options.MinPortNullable);
            Assert.AreEqual(12999, player.Options.MaxPortNullable);
            Assert.AreEqual(3000, player.Options.TimeoutNullable);
            Assert.AreEqual(false, player.Options.AutoStart);
            Assert.AreEqual(3, player.Options.RetriesNullable);

            // Process Info
            Assert.IsNotNull(player.MyProcessInfo);
            Assert.AreEqual(0, player.MyProcessInfo.ProcessId);
            Assert.AreEqual(player.Options.Label, player.MyProcessInfo.Label);
            Assert.AreEqual(ProcessInfo.ProcessType.Player, player.MyProcessInfo.Type);
            Assert.AreEqual(ProcessInfo.StatusCode.NotInitialized, player.MyProcessInfo.Status);
            Assert.AreEqual("127.0.0.1", player.MyProcessInfo.EndPoint.Host);
            Assert.AreEqual(player.MyCommunicator.Port, player.MyProcessInfo.EndPoint.Port);

            // Player Conversation Factory
            Assert.IsNotNull(player.CommSubsystem.ConversationFactory);

        }

        public static void AppConfigsReplacement(Player.Player player)
        {
            player.MyIdentity.FirstName = "James";
            player.MyIdentity.LastName = "Tester";
            player.MyIdentity.ANumber = "A00003456";
            player.MyIdentity.Alias = "T-Rantula";
            player.RegistryEP = new PublicEndPoint("127.0.0.1:12000");
        }
    }
}
