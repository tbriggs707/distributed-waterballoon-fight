﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using log4net;

namespace BalloonStore.Conversations
{
    class GetPublicKeyConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(GetPublicKeyConversation));

        public int SubjectProcessId { get; internal set; }

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(PublicKeyReply) };
            }
        }

        protected override Message CreateRequest()
        {
            return new GetKeyRequest
            {
                ProcessId = SubjectProcessId
            };
        }

        protected override void ProcessReply(Reply reply)
        {
            PublicKeyReply keyReply = reply as PublicKeyReply;
            Logger.Debug("Reply: " + keyReply.GetType().Name + " Success: " + keyReply.Success);

            if (keyReply.Success)
                (Process as BalloonStore).KnownProcesses.AddPublicKey(SubjectProcessId, keyReply.Key);
        }
    }
}
