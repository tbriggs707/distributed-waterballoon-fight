﻿using CommSub;
using log4net;
using Messages.RequestMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStore.Conversations
{
    public class ShutdownConversation : Conversation
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ShutdownConversation));

        public override void Execute(object context = null)
        {
            Done = false;

            if (PreExecuteAction != null)
                PreExecuteAction(context);

            Process.BeginShutdown();

            if (Error != null)
                Logger.Warn(Error.Message);

            Done = true;

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }

        protected override bool IsValidRemoteProcess(int processId)
        {
            return processId == 1;
        }
    }
}
