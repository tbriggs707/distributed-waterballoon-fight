﻿using CommSub.Conversations.InitiatorConversations;
using log4net;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStore.Conversations
{
    public class JoinGameConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(JoinGameConversation));
        private BalloonStore Store
        {
            get
            {
                return Process as BalloonStore;
            }
        }
        public GameInfo Game { get; set; }

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(JoinGameReply) };
            }
        }

        protected override Message CreateRequest()
        {
            JoinGameRequest request = new JoinGameRequest
            {
                GameId = Game.GameId,
                Process = Store.MyProcessInfo
            };

            Store.MyProcessInfo.Status = ProcessInfo.StatusCode.JoiningGame;
            return request;
        }

        protected override void ProcessReply(Reply reply)
        {
            JoinGameReply joinReply = reply as JoinGameReply;
            Store.MyProcessInfo.Status = ProcessInfo.StatusCode.JoinedGame;
            Store.CurrGameInfo = Game;

            Store.KnownProcesses.AddOrUpdate(new GameProcessData
            {
                Type = Store.MyProcessInfo.Type,
                ProcessId = Store.MyProcessInfo.ProcessId
            });

            Store.ProcessData.LifePoints = joinReply.InitialLifePoints;
            Logger.Debug("JoinGameConversation - ProcessReply()\nLifePoints initialized to " + Store.ProcessData.LifePoints);
        }

        protected override void PostFailureAction()
        {
            Logger.Debug("JoinGameConversation.cs PostFailureAction");
        }
    }
}
