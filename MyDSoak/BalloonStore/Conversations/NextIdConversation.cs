﻿using CommSub.Conversations.InitiatorConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;

namespace BalloonStore.Conversations
{
    class NextIdConversation : RequestReply
    {
        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(NextIdReply) };
            }
        }

        private BalloonStore Store
        {
            get
            {
                return Process as BalloonStore;
            }
        }
        public int NextID { get; set; }
        public int NumberOfIDs { get; set; }

        protected override Message CreateRequest()
        {
            return new NextIdRequest();
        }

        protected override void ProcessReply(Reply reply)
        {
            if (reply == null) return;

            NextIdReply nextReply = reply as NextIdReply;
            Store.NextID = nextReply.NextId;
            Store.AllottedBalloonCount = nextReply.NumberOfIds;
        }
    }
}
