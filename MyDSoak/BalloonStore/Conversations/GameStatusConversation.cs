﻿using CommSub;
using log4net;
using Messages;
using Messages.RequestMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStore.Conversations
{
    public class GameStatusConversation : Conversation
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(GameStatusConversation));
        private BalloonStore Store
        {
            get
            {
                return Process as BalloonStore;
            }
        }

        public override void Execute(object context = null)
        {
            Logger.Debug("GameStatusConversation Execute()");
            if (PreExecuteAction != null)
                PreExecuteAction(context);

            GameStatusNotification notification = IncomingEnvelope.ActualMessage as GameStatusNotification;

            Store.SaveGameStatus(notification);

            if (PostExecuteAction != null)
                PostExecuteAction(context);
        }
    }
}
