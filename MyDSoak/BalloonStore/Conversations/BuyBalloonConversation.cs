﻿using CommSub.Conversations.ResponderConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using log4net;
using SharedObjects;

namespace BalloonStore.Conversations
{
    class BuyBalloonConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(BuyBalloonConversation));
        private BalloonStore Store
        {
            get
            {
                return Process as BalloonStore;
            }
        }

        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(BuyBalloonRequest) };
            }
        }

        protected override Message CreateReply()
        {
            Logger.Debug("Received and processed BalloonRequest from " + Request.MsgId.Pid);

            BalloonReply reply = new BalloonReply() { Success = false };
            BuyBalloonRequest request = Request as BuyBalloonRequest;

            // What if the game hasn't started yet?
            // What if I reserve a balloon and I end up not using it?
            if (Store.CurrGameInfo.Status != GameInfo.StatusCode.InProgress)
                reply.Note = "You are not in the Game";
            else if (!Store.ValidatePennies(new Penny[] { request.Penny } ))
                reply.Note = "Invalid Penny";
            else
            {
                if (Store.UnfilledBalloons.AvailableCount < 1)
                    reply.Note = "No balloons left in inventory";
                else
                {
                    Balloon balloon = Store.UnfilledBalloons.ReserveOne();
                    if (balloon == null)
                        reply.Note = "No balloons left in inventory";
                    else
                    {
                        reply.Balloon = balloon;
                        reply.Success = true;
                    }
                }
            }

            return reply;
        }
    }
}
 
