﻿using CommSub.Conversations.InitiatorConversations;
using log4net;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using System;

namespace BalloonStore.Conversations
{
    public class LogoutConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(LogoutConversation));

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(Reply) };
            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("Logout request has been created");
            return new LogoutRequest();
        }

        protected override void ProcessReply(Reply reply)
        {
            // what do I do here...?
            Logger.Debug("Logged Out");
        }
    }
}
