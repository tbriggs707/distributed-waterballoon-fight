﻿using CommSub.Conversations.InitiatorConversations;
using log4net;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStore.Conversations
{
    public class ValidatePennyConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ValidatePennyConversation));

        public Penny[] Pennies { get; set; }
        public bool Used { get; set; }
        public bool Valid { get; set; }

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(Reply) };
            }
        }

        protected override Message CreateRequest()
        {
            return new PennyValidation
            {
                Pennies = Pennies,
                MarkAsUsedIfValid = Used
            };
        }

        protected override void ProcessReply(Reply reply)
        {
            Logger.Debug("Penny Validated: " + reply.Success);
            Valid = reply.Success;
        }
    }
}
