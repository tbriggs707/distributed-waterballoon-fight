﻿using CommSub.Conversations.InitiatorConversations;
using System;
using Messages;
using log4net;
using Messages.ReplyMessages;
using Messages.RequestMessages;

namespace BalloonStore.Conversations
{
    public class LeaveGameConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(LeaveGameConversation));
        private BalloonStore Store
        {
            get
            {
                return Process as BalloonStore;
            }
        }

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(Reply) };
            }
        }

        protected override Message CreateRequest()
        {
            Logger.Debug("LeaveGame Request created");
            return new LeaveGameRequest();
        }

        protected override void ProcessReply(Reply reply)
        {
            Logger.Debug("LeaveGame ProcessReply()");
            Store.CurrGameInfo = null;
            base.ProcessReply(reply);
        }
    }
}
