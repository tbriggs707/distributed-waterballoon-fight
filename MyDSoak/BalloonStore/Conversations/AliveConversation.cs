﻿using CommSub.Conversations.ResponderConversations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;

namespace BalloonStore.Conversations
{
    class AliveConversation : RequestReply
    {
        protected override Type[] AllowedTypes
        {
            get
            {
                return new[] { typeof(AliveRequest) };
            }
        }

        protected override Message CreateReply()
        {
            return new Reply() { Success = true };
        }
    }
}
