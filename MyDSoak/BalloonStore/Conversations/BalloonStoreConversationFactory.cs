﻿using CommSub;
using Messages.RequestMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStore.Conversations
{
    public class BalloonStoreConversationFactory : ConversationFactory
    {
        public override void Initialize()
        {
            Add(typeof(AliveRequest), typeof(AliveConversation));
            Add(typeof(BuyBalloonRequest), typeof(BuyBalloonConversation));
            Add(typeof(ShutdownRequest), typeof(ShutdownConversation));
            Add(typeof(GameStatusNotification), typeof(GameStatusConversation));
        }
    }
}
