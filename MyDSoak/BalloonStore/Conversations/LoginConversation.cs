﻿using CommSub.Conversations.InitiatorConversations;
using log4net;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStore.Conversations
{
    public class LoginConversation : RequestReply
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(LoginConversation));

        protected override Type[] AllowedReplyTypes
        {
            get
            {
                return new[] { typeof(LoginReply) };
            }
        }
        public BalloonStore Store
        {
            get
            {
                return Process as BalloonStore;
            }
        }
        public ProcessInfo.ProcessType ProcessType { get; set; }
        public string ProcessLabel { get; set; }
        public IdentityInfo Identity { get; set; }
        public PublicKey PublicKey { get; set; }

        protected override Message CreateRequest()
        {
            Logger.Debug("LoginRequest being created");
            return new LoginRequest
            {
                ProcessType = ProcessType,
                ProcessLabel = ProcessLabel,
                Identity = Identity,
                PublicKey = PublicKey
            };
        }

        protected override void ProcessReply(Reply reply)
        {
            Logger.Debug("PublicEndPoint: " + Store.MyProcessInfo.EndPoint);
            LoginReply loginReply = reply as LoginReply;
            Store.AssignLoginInfo(loginReply);
        }
    }
}
