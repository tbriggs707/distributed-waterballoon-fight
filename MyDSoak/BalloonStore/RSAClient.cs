﻿using BalloonStore.Conversations;
using CommSub;
using log4net;
using SharedObjects;
using System.Security.Cryptography;
using Utils;

namespace BalloonStore
{
    public class RSAClient
    {
        #region Private Variables
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RSAClient));
        private static readonly SHA1Managed hasher = new SHA1Managed();
        private RSAPKCS1SignatureFormatter signer;
        #endregion
        #region Public Variables
        public delegate RSACryptoServiceProvider RsaLookupMethod(int processId);
        public RSAPKCS1SignatureFormatter MySigner
        {
            set
            {
                signer = value;
                if (signer != null)
                    signer.SetHashAlgorithm("SHA1");
            }
        }
        public CommSubsystem CommSubsystem { get; set; }
        public PublicEndPoint RegistryEndPoint { get; set; }
        public RsaLookupMethod RSALookup { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a signature for any resource (balloons)
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        public byte[] CreateDigitalSignature(SharedResource resource)
        {
            if (resource == null || signer == null) return null;

            Logger.Debug("A digital signature for a " + resource.GetType().Name + ", ID: " + resource.Id + " has been created");
            byte[] data = resource.DataBytes();
            byte[] message = hasher.ComputeHash(data);
            return signer.CreateSignature(message);
        }

        /// <summary>
        /// Validates the signature of any resource (balloons)
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        public bool ValidateSignature(SharedResource resource)
        {
            bool verified = false;

            if (resource != null)
            {
                Logger.Debug("Signature for: " + resource.GetType().Name + " with ID: " + resource.Id);
                Logger.Debug("Signed by: " + resource.SignedBy);

                RSACryptoServiceProvider rsa = FindRSA(resource.SignedBy);
                if (rsa != null)
                {
                    byte[] data = resource.DataBytes();
                    byte[] message = hasher.ComputeHash(data);

                    Logger.Debug("ValidateSignature(), dataBytes:" + HelperFunctions.ByteToStringDisplay(data));
                    Logger.Debug("ValidateSignature(), hash: " + HelperFunctions.ByteToStringDisplay(message));

                    RSAPKCS1SignatureDeformatter comparer = new RSAPKCS1SignatureDeformatter(rsa);
                    comparer.SetHashAlgorithm("SHA1");
                    verified = comparer.VerifySignature(message, resource.DigitalSignature);

                    Logger.Debug("ValidateSignature(), verified: " + verified);
                }
            }
            return verified;
        }

        /// <summary>
        /// Finds the RSA given a process ID, if the RSA isn't found a conversation is sent to the Registry
        /// to get it
        /// </summary>
        /// <param name="processId"></param>
        /// <returns></returns>
        private RSACryptoServiceProvider FindRSA(int processId)
        {
            RSACryptoServiceProvider rsa = RSALookup(processId);

            // If RSA exists, return it. If it doesn't, create conversation then look for it.
            if (rsa == null && CommSubsystem != null)
            {
                Logger.Warn("Process " + processId + " has no RSA");

                GetPublicKeyConversation convo = CommSubsystem.ConversationFactory.CreateFromConversationType<GetPublicKeyConversation>();
                convo.SubjectProcessId = processId;
                convo.TargetEndPoint = RegistryEndPoint;
                convo.Execute();
                rsa = RSALookup(processId);

                Logger.Debug("RSA retrieval was successful for " + processId);
            }
            else
                Logger.Debug("RSA has already been created for process " + processId);

            return rsa;
        }
        #endregion
    }
}
