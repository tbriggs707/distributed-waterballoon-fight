﻿using BalloonStore.Conversations;
using CommSub;
using log4net;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;

namespace BalloonStore
{
    public class BalloonStore : CommProcess
    {
        #region Private Variables
        private static readonly ILog Logger = LogManager.GetLogger(typeof(BalloonStore));
        private Conversation.ActionHandler generateBalloonsAction;
        private RSAClient signerValidator;
        private readonly ResourceSet<Balloon> unfilledBalloons = new ResourceSet<Balloon>();
        private readonly ResourceSet<Penny> myPennies = new ResourceSet<Penny>();
        private readonly KnownGameProcesses gameProcess = new KnownGameProcesses();
        #endregion
        #region Protected Variables
        protected bool ReadyToLeaveGame
        {
            get
            {
                return MyProcessInfo.Status == ProcessInfo.StatusCode.JoiningGame || MyProcessInfo.Status == ProcessInfo.StatusCode.JoinedGame || MyProcessInfo.Status == ProcessInfo.StatusCode.PlayingGame;
            }
        }
        protected bool ReadyToLogout
        {
            get
            {
                return MyProcessInfo.Status != ProcessInfo.StatusCode.NotInitialized || MyProcessInfo.Status != ProcessInfo.StatusCode.Unknown;
            }
        }
        protected bool StillPlaying
        {
            get
            {
                return MyProcessInfo.Status != ProcessInfo.StatusCode.Terminating && KeepGoing;
            }
        }
        protected RSAPKCS1SignatureFormatter dSignatureCreator;
        #endregion
        #region Public Variables
        public int AllottedBalloonCount { get; set; }
        public GameProcessData ProcessData
        {
            get
            {
                GameProcessData result = null;

                if (gameProcess != null && MyProcessInfo != null)
                    result = gameProcess[MyProcessInfo.ProcessId];

                return result;
            }
        }
        public RSACryptoServiceProvider RSA;
        public PublicKey MyPublicKey;
        public ResourceSet<Balloon> UnfilledBalloons
        {
            get
            {
                return unfilledBalloons;
            }
        }
        public new BalloonStoreRuntimeOptions Options { get; set; }
        public GameInfo CurrGameInfo { get; set; }
        public int NextID { get; set; }
        public IdentityInfo Identity { get; set; }
        public KnownGameProcesses KnownProcesses
        {
            get
            {
                return gameProcess;
            }
        }
        public RSAClient SigVal
        {
            get
            {
                return signerValidator;
            }
        }
        #endregion

        /// <summary>
        /// This is the main loop, this controls what happens when.
        /// </summary>
        /// <param name="state"></param>
        protected override void Process(object state)
        {
            try
            {
                Logger.Debug("Starting Balloon Store's main Process() loop");
                Initialize();

                while (StillPlaying)
                {
                    if (Paused())
                    {
                        Thread.Sleep(4500);
                        MyProcessInfo.Status = ProcessInfo.StatusCode.Registered;
                    }
                    else if (Initializing())
                        TryLogin(postAction: generateBalloonsAction);
                    else if (Registered())
                        TryJoinGame();
                    else if (DonePlaying())
                        MyProcessInfo.Status = ProcessInfo.StatusCode.Terminating;
                    else if (GameStarted())
                        ExitWhenEmpty();
                    Thread.Sleep(1000);
                }

                MyProcessInfo.Status = ProcessInfo.StatusCode.Terminating;
                
                if (ReadyToLeaveGame)
                    TryLeaveGame();

                if (ReadyToLogout)
                    TryLogout();

                Stop();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
            }
        }

        #region Try Methods
        /// <summary>
        /// Creates a login conversation that tries to login to the Registry
        /// </summary>
        /// <param name="preAction">A method to call before logging in</param>
        /// <param name="postAction">A method to call after logging in</param>
        public void TryLogin(Conversation.ActionHandler preAction = null, Conversation.ActionHandler postAction = null)
        {
            Logger.Debug("Logging into Registry: " + RegistryEndPoint.HostAndPort);
            Logger.Debug("Identity: " + Identity.FirstName + " " + Identity.LastName + " " + Identity.ANumber);
            Logger.Debug("PublicKey: " + MyPublicKey.ToString());

            LoginConversation convo = CommSubsystem.ConversationFactory.CreateFromConversationType<LoginConversation>();
            convo.TargetEndPoint = RegistryEndPoint;
            convo.Identity = Identity;
            convo.ProcessLabel = MyProcessInfo.Label;
            convo.ProcessType = ProcessInfo.ProcessType.BalloonStore;
            convo.PublicKey = MyPublicKey;
            convo.PreExecuteAction = preAction;
            convo.PostExecuteAction = postAction;
            convo.Execute();
        }

        /// <summary>
        /// Creates a join game conversation that tries to join the game created by the Game Manager who initiated the Balloon Store
        /// </summary>
        public void TryJoinGame()
        {
            Console.WriteLine("Joining Game: " + CurrGameInfo.GameManagerId);
            Logger.Debug("Joining Game: " + CurrGameInfo.GameManagerId);

            if (CurrGameInfo != null)
            {
                JoinGameConversation convo = CommSubsystem.ConversationFactory.CreateFromConversationType<JoinGameConversation>();
                convo.Game = CurrGameInfo;
                convo.ToProcessId = CurrGameInfo.GameManagerId;
                convo.Execute();
            }
        }

        public void TryGetNextId()
        {
            Logger.Debug("Getting NextID from the registry");

            NextIdConversation convo = CommSubsystem.ConversationFactory.CreateFromConversationType<NextIdConversation>();
            convo.NumberOfIDs = Options.Balloons;
            convo.TargetEndPoint = RegistryEndPoint;
            convo.Execute();
        }

        /// <summary>
        /// Creates a leave game conversation that tries to leave the current game
        /// </summary>
        public void TryLeaveGame()
        {
            Logger.Debug("Leaving the current game");

            if (CurrGameInfo != null)
            {
                LeaveGameConversation convo = CommSubsystem.ConversationFactory.CreateFromConversationType<LeaveGameConversation>();
                convo.ToProcessId = CurrGameInfo.GameManagerId;
                convo.Execute();
            }
        }

        /// <summary>
        /// Creates a logout conversation that tries to logout via the registry
        /// </summary>
        public void TryLogout()
        {
            Logger.Debug("Logging out");
            LogoutConversation convo = CommSubsystem.ConversationFactory.CreateFromConversationType<LogoutConversation>();
            convo.TargetEndPoint = RegistryEndPoint;
            convo.Execute();
        }
        #endregion

        #region Other Methods
        #region Loop Methods
        /// <summary>
        /// Returns true if the status is 'Pausing'
        /// </summary>
        /// <returns></returns>
        private bool Paused()
        {
            return MyProcessInfo.Status == ProcessInfo.StatusCode.Pausing;
        }

        /// <summary>
        /// Returns true if the status is 'Initializing'
        /// </summary>
        /// <returns></returns>
        private bool Initializing()
        {
            return MyProcessInfo.Status == ProcessInfo.StatusCode.Initializing;
        }

        /// <summary>
        /// Returns true if the status is 'Registered'
        /// </summary>
        /// <returns></returns>
        private bool Registered()
        {
            return MyProcessInfo.Status == ProcessInfo.StatusCode.Registered;
        }

        /// <summary>
        /// Returns true if the status is 'PlayingGame'
        /// </summary>
        /// <returns></returns>
        private bool GameStarted()
        {
            return MyProcessInfo.Status == ProcessInfo.StatusCode.PlayingGame;
        }

        /// <summary>
        /// Returns true if the status is on of the following: won, lost, tied
        /// </summary>
        /// <returns></returns>
        private bool DonePlaying()
        {
            return MyProcessInfo.Status == ProcessInfo.StatusCode.Won || MyProcessInfo.Status == ProcessInfo.StatusCode.Lost || MyProcessInfo.Status == ProcessInfo.StatusCode.Tied;
        }

        /// <summary>
        /// Returns true if the current game is in progress and if the balloon store is joining/ed the game
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private bool InProgress(GameStatusNotification status)
        {
            return status.Game.Status == GameInfo.StatusCode.InProgress && (MyProcessInfo.Status == ProcessInfo.StatusCode.JoiningGame || MyProcessInfo.Status == ProcessInfo.StatusCode.JoinedGame);
        }

        /// <summary>
        /// Returns true if the current game has ended
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private bool GameOver(GameStatusNotification status)
        {
            return status.Game.Status == GameInfo.StatusCode.Ending || status.Game.Status == GameInfo.StatusCode.Complete;
        }
        #endregion

        #region Start/End Methods
        private void Initialize()
        {
            Logger.Debug("BalloonStore Initialize()");

            RegistryEndPoint = new PublicEndPoint() { HostAndPort = Options.Registry };
            generateBalloonsAction = new Conversation.ActionHandler(SetupBalloons);
            RSA = new RSACryptoServiceProvider();
            RSAParameters senderPublicKeyInfo = RSA.ExportParameters(true);

            MyPublicKey = new PublicKey
            {
                Exponent = senderPublicKeyInfo.Exponent,
                Modulus = senderPublicKeyInfo.Modulus
            };

            signerValidator = new RSAClient
            {
                RegistryEndPoint = RegistryEndPoint,
                RSALookup = new RSAClient.RsaLookupMethod(KnownProcesses.GetProcessRsa),
                MySigner = new RSAPKCS1SignatureFormatter(RSA)
            };

            signerValidator.CommSubsystem = CommSubsystem;

            Identity = new IdentityInfo
            {
                ANumber = Options.ANumber,
                FirstName = Options.FirstName,
                LastName = Options.LastName,
                Alias = Options.Alias
            };

            CurrGameInfo = new GameInfo
            {
                GameId = Options.GameId,
                GameManagerId = Options.GameManagerId
            };

            BalloonStoreConversationFactory conversationFactory = new BalloonStoreConversationFactory
            {
                Process = this,
                DefaultMaxRetries = Options.Retries,
                DefaultTimeout = Options.Timeout
            };

            GameProcessData gameMan = new GameProcessData
            {
                ProcessId = Options.GameManagerId,
                Type = ProcessInfo.ProcessType.GameManager
            };

            MyProcessInfo = new ProcessInfo
            {
                Label = Options.Label,
                Type = ProcessInfo.ProcessType.BalloonStore,
                Status = ProcessInfo.StatusCode.Initializing
            };

            KnownProcesses.AddOrUpdate(gameMan);
            SetupCommSubsystem(conversationFactory, Options.MinPort, Options.MaxPort);
        }

        public override void Start()
        {
            Logger.Debug("Game Process Start()");
            Initialize();
            base.Start();
        }

        public override void CleanupSession()
        {
            Logger.Debug("BalloonStore CleanupSession()");

            if (ProcessData != null)
                ProcessData.LifePoints = 0;

            if (CurrGameInfo != null)
                CurrGameInfo = null;

            base.CleanupSession();
        }
        #endregion
        
        #region Conversation Methods
        /// <summary>
        /// Takes the results from the LoginReply and assigns them to the appropriate objects
        /// </summary>
        /// <param name="loginReply"></param>
        public void AssignLoginInfo(LoginReply loginReply)
        {
            if (loginReply != null && loginReply.Success && loginReply.ProcessInfo != null)
            {
                MyProcessInfo = loginReply.ProcessInfo;
                MessageNumber.LocalProcessId = MyProcessInfo.ProcessId;

                ProxyEndPoint = loginReply.ProxyEndPoint;
                PennyBankEndPoint = loginReply.PennyBankEndPoint;
                KnownProcesses.AddPublicKey(1, loginReply.PennyBankPublicKey);
            }
        }

        /// <summary>
        /// Used to sign/validate each balloon
        /// </summary>
        /// <param name="context"></param>
        public void SetupBalloons(object context)
        {
            Logger.Debug("BalloonStore CreateBalloons()");

            TryGetNextId();
            // Replace Options.Balloons with AllottedBalloonCount (comes from the NextIdReply)
            for (int i = NextID; i < Options.Balloons + NextID; i++)
            {
                Balloon balloon = new Balloon
                {
                    Id = i,
                    SignedBy = MyProcessInfo.ProcessId
                };

                balloon.DigitalSignature = SigVal.CreateDigitalSignature(balloon);
                Logger.Debug("Balloon created, ID " + balloon.Id);
                //Logger.Debug("Digital Signature: " + HelperFunctions.ByteToStringDisplay(balloon.DigitalSignature));
                Logger.Debug("Signed by: " + balloon.SignedBy);
                unfilledBalloons.AddOrUpdate(balloon);
            }
        }

        /// <summary>
        /// Used to safely re-assign the status and log that a change in status occurred
        /// </summary>
        /// <param name="newState"></param>
        public void ChangeGameState(GameInfo.StatusCode newState)
        {
            if (CurrGameInfo != null && CurrGameInfo.Status != newState)
            {
                lock (MyLock)
                {
                    CurrGameInfo.Status = newState;
                    Logger.Debug("Change Game Status to " + newState);
                }
            }
        }

        /// <summary>
        /// Used to detect if a shutdown is needed, used to update the status
        /// </summary>
        /// <param name="status"></param>
        public void SaveGameStatus(GameStatusNotification status)
        {
            gameProcess.UpdateProcesses(status.Game.CurrentProcesses);
            ChangeGameState(status.Game.Status);

            if (GameOver(status))
                MyProcessInfo.Status = ProcessInfo.StatusCode.Terminating;
            else if (InProgress(status))
            {
                if (KnownProcesses[MyProcessInfo.ProcessId] == null)
                    RaiseShutdownEvent();
                else
                    MyProcessInfo.Status = ProcessInfo.StatusCode.PlayingGame;
            }
        }

        /// <summary>
        /// Validates a list of pennies with the Penny Bank
        /// </summary>
        /// <param name="pennies"></param>
        /// <param name="markAsUsed"></param>
        /// <returns></returns>
        public bool ValidatePennies(Penny[] pennies)
        {
            Logger.Debug("Validating pennies");
            bool pennyIsValid = false;

            // Only send the conversation if ALL pennies: exist, haven't been used, 
            // and have been validated by the Penny Bank
            if (pennies == null || pennies.Length == 0)
                Logger.Debug("Zero pennies, nothing to validate");
            else if (myPennies.AreAnyUsed(pennies))
                Logger.Debug("One of the pennies has already been used");
            else if (!pennies.All(p => SigVal.ValidateSignature(p)))
                Logger.Debug("Invalid penny signature found");
            else
            {
                ValidatePennyConversation convo = CommSubsystem.ConversationFactory.CreateFromConversationType<ValidatePennyConversation>();
                convo.Pennies = pennies;
                convo.Used = true;
                convo.TargetEndPoint = PennyBankEndPoint;
                convo.Execute();
                pennyIsValid = convo.Valid;
            }

            Logger.Debug("Penny validated: " + pennyIsValid);
            return pennyIsValid;
        }

        /// <summary>
        /// Used to shutdown when all available balloons are gone
        /// </summary>
        protected void ExitWhenEmpty()
        {
            if (UnfilledBalloons.AvailableCount < 1)
            {
                RaiseShutdownEvent();
                Environment.Exit(0);
            }
        }

        public override void Stop()
        {
            base.Stop();
            Environment.Exit(0);
        }
        #endregion
        #endregion
    }
}
