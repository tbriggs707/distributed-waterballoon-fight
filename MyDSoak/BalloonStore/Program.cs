﻿using log4net;
using log4net.Config;
using System;
using System.Linq;
using System.Threading;

namespace BalloonStore
{
    class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));

        private static void Main(string[] args)
        {
            try
            {
                BalloonStoreRuntimeOptions options = new BalloonStoreRuntimeOptions();
                XmlConfigurator.Configure();

                options.SetDefaults();
                BalloonStore store = GetStore(options, args);
                Logger.Debug("Balloon Store Main Program");

                if (store != null)
                {
                    store.Start();

                    Console.WriteLine("Press the 'Enter' key to stop playing and logout...");
                    Console.ForegroundColor = ConsoleColor.Yellow;

                    while (Console.ReadKey(true).Key != ConsoleKey.Enter)
                    {
                        Console.WriteLine(" Press 'Enter' to exit");
                        Thread.Sleep(100);
                    }

                    store.Stop();
                }
                EndMessage();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Console.WriteLine(exception.Message);
            }
        }

        private static BalloonStore GetStore(BalloonStoreRuntimeOptions options, string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            BalloonStore store = null;
            if (args.Count() == 0) return store;

            if (args.Count() == 5)
            {
                int gameManID = Convert.ToInt16(args[0].Replace("--gmid=", ""));
                int gameID = Convert.ToInt16(args[1].Replace("--gameid=", ""));
                int balloons = Convert.ToInt16(args[2].Replace("--balloons=", ""));
                string registry = args[3].Replace("--registry=", ""); 
                string label = "'s Balloon Store # " + args[4].Replace("--storeindex=", "");

                // Added this for testing purposes
                Logger.Debug("Command-line option --gmid: " + gameID);
                Logger.Debug("Command-line option --gameid: " + gameManID);
                Logger.Debug("Command-line option --balloons: " + balloons);
                Logger.Debug("Command-line option --registry: " + registry);
                Logger.Debug("Command-line option --storeindex: " + label);

                options.GameId = gameID;
                options.GameManagerId = gameManID;
                options.Balloons = balloons;
                options.Registry = registry;
                options.Label += label;

                store = new BalloonStore()
                {
                    Options = options,
                    Label = options.Label
                };
            }

            return store;
        }

        private static void EndMessage()
        {
            Logger.Debug("Program Terminated");
            Console.ResetColor();
            Console.WriteLine("Program Terminating...\nThanks for playing!");
            Thread.Sleep(1500);
        }
    }
}
