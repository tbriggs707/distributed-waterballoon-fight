﻿using CommSub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BalloonStore
{
    public class BalloonStoreRuntimeOptions : RuntimeOptions
    {
        public int GameId { get; set; }
        public int GameManagerId { get; set; }
        public int Balloons { get; set; }

        public override void SetDefaults()
        {
            Registry = "127.0.0.1:12000";
            ANumber = "A00003456";
            FirstName = "James";
            LastName = "Tester";
            Alias = "T-Rantula";
            Label = "T-Rantula";
            MinPortNullable = 12000;
            MaxPortNullable = 12999;
            TimeoutNullable = 3000;
            AutoStart = false;
            RetriesNullable = 3;
        }
    }
}
