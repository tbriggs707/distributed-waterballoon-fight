Learning how a distributed system works. The Player, Registry, Game Manager, 
Balloon Store, Umbrella Supplier, and Water Source are separate programs that
simulate a distributed system. The player can be on a separate computer and
network so long as the IP Address is configured properly for all the game
programs.

Communication Protocols: Recieving pennies uses TCP, all other programs use UDP.

The player is configured with some AI to know what to bid for umbrellas, when to
fill & throw waterballoons and when to activate an umbrella.